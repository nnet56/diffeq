# Objectives

Simple scripts to help work with differential equations for my bachelor math
courses.

Motivation was to :
1. Visualize global solutions for DEs.
2. Draw isoclines.
3. Try the Julia programming language.

# Requirements

- Julia (tested with version 1.9.4)
- GLMakie (tested with version 0.9.8)
- Makie (tested with version 0.20.7)

# Usage

Modify the variables at the beginning of the scripts and run.
