#=
# isoclines.jl
#
# Author: Marc MELKONIAN
# Modified at: Sat Feb 10 10:37:56 PM CET 2024
#
# A simple julia script to draw isoclines and vector field for a differential
# equation.
=#

using GLMakie
using CairoMakie
GLMakie.activate!(focus_on_show = true)

# global vars
ts = -3:.2:3 # x axis
ys = -3:.2:3 # y axis
length = .025      # length of vectors in vector field
f_prime(y,t) = y^2 + t^2

fig = Figure(size=(800,800))
Axis(fig[1,1],
     aspect=1,
     xticklabelsize=18.0f0,
     yticklabelsize=18.0f0
)

# draw isoclines
for i in (0,2,3)
    if i == 0
	    poly!(Circle(Point2f(0, 0), .05), color = :purple)
    end
    arc!(Point2f(0), sqrt(i), -pi, pi, linewidth=3)
end

# draw vector field
ps = [Point2f(x, y) for x in ts for y in ys]
# `arrows` 2nd arg is relative to the first arg
ns = map(p -> Point2f(sqrt(length / (1 + f_prime(p[2],p[1])^2)),
					  sqrt(length / ( 1 + 1/f_prime(p[2],p[1])^2 ))),
		 ps)
arrows!(ps,
        ns,
		linewidth = 1,
        arrowsize = 7,
        align = :origin,
		color = "#333333"
)

# save("../ex1.png", fig, px_per_unit = 2; backend = CairoMakie)
save("../exports/ex1.png", fig, px_per_unit = 2)
