#=
# integral_curves.jl
#
# Author: Marc MELKONIAN
# Modified at: Tue Feb 13 10:35:22 PM CET 2024
#
# A simple script to draw integral curves of a given differential equation
=#

using GLMakie
using CairoMakie
GLMakie.activate!(focus_on_show = true)

# global vars
lambdas = -3:1:3  # lambda value in the global solution of the DE
xs = -30:0.05:30   # x axis


fig = Figure(size=(800,800))
Axis(fig[1,1],
     xticklabelsize=18.0f0,
     yticklabelsize=18.0f0
)
xlims!(low = -15, high = 15)
ylims!(low = -15, high = 15)

# global solution for DE must be changed here
# NOTE: `log` is ln (base e) in Julia
function f(lambda, x)
    return (lambda + log(abs(x)))*(1/(1-(1/x^2)))
end


# draw isoclines
for l in lambdas
    lines!(xs, f.(l, xs))
end

save("../exports/ex2.png", fig, px_per_unit=2)
